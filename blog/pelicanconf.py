#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

SITEURL = ''
HOMEURL = ''
SITENAME = "Ben Tremblay's Blog"
AUTHOR = 'Benjamin HS Tremblay'
PROFILE_IMAGE = 'profile.png'
BIO = """Hi, I'm Ben. I'm a Software Engineer living in Chilliwack, BC."""

PATH = 'content'

TIMEZONE = 'America/Vancouver'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
        ('Pelican', 'http://getpelican.com/'),
        ('Python.org', 'http://python.org/'),
        ('Jinja2', 'http://jinja.pocoo.org/'),
)

# Social widget
SOCIAL = (
        ('gitlab', 'https://gitlab.com/bhs.tremblay'),
        ('envelope', 'blog@tremblay.dev'),
        ('home', 'https://tremblay.dev'),
)

DEFAULT_PAGINATION = 10
DEFAULT_DATE = 'fs'

STATIC_PATHS = ['images', 'extra/favicon.ico']
EXTRA_PATH_METADATA = {'extra/favicon.ico': {'path': 'favicon.ico'},}
